# Dynamisez vos sites web avec JavaScript

[lien du cours : OpenClassRoom](https://openclassrooms.com/fr/courses/1916641-dynamisez-vos-sites-web-avec-javascript)

## 1-Introduction au JS
## 2-Premier pas en JS
## 3-Variables
## 4-Conditions
## 5-Boucles
## 6-fonctions
## 7-Objets et Tableaux
## 8-Convertion de nombres en lettres
## 9-Manipulation de code HTML
## 10-Évenements
## 11-Formulaires
## 12-Manipulation de code CSS
## 13-Les objets
## 14-Chaines de caractères
## 15-Expressions réguliaires
## 16-Donnéess numériques
## 17-Gestion du temps
## 18-Les tableaux
## 19-Les images
## 20-Polyfills & Wrapper
## 21-Les closures
## 22-AJAX
## 23-XMLHttpRequest
## 24-Upload via iframe
## 25-Dynamic Script Loading
## 26-Audio & Video
## 27-Canvas
## 28-API File
## 29-Drag & Drop